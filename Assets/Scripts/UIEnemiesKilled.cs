﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEnemiesKilled : MonoBehaviour
{

    //Used to alter text at will.
    public Text enemieskilled;

    //The objective enemy limit
    public static int enemylimit = 10;

    private void Update()
    {
        enemieskilled.text = "Enemies Killed:" + PlayerController.enemieskilled + "/" + enemylimit; 

        //Change text if player completes objective
        if (PlayerController.enemieskilled == enemylimit)
        {
            enemieskilled.text = "Objective Complete: The boss finds you worthy";
        }


    }
}

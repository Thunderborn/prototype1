﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    //Health
    public int health;
    public int numofHeart;

    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    //Used to ensure death sound plays once.
    public static bool deathsoundplay;

    //Timer that allows you to regain health every 10 seconds.
    private float recoverytimer = 0f;
    private float recoverydelay = 10f;

    //Animation.
    public Animator animator;
    //Death State
    const string DEATH = "Death";


    private void Start()
    {
        //Assigning variables.
        deathsoundplay = false;

        //Assigned this to this script because the Ambience stops when we die.
        //This script instantiates when player is alive hence why I used this script instead of others.

        AudioManager.instance.Play("Ambience");
    }

    // Update is called once per frame
    void Update()
    {
        //Do constant checks for any of these events.
        HealthSet();
        Die();
        HealthRecovery();

    }

    //Simple damage function that damages by parameter
    public void Damage(int damage)
    {
        health -= damage;
    }


    //Function that kills player if health is 0 and plays death noise.
    public void Die()
    {
        //Death
        if (health <= 0 && this.gameObject.CompareTag("Player"))
        {

            animator.Play(DEATH);
            if (!deathsoundplay)
            {
                AudioManager.instance.Play("Death");
                deathsoundplay = true;
                

            }




        }
    }

    private void HealthSet()
    {

        //Set the health to the numofheart if its bigger than it. 
        if (health > numofHeart)
        {
            health = numofHeart;
        }

        //Change sprites and enable/disable depending on index, health and numOfHeart values
        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < health)
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
                
            }


            if (i < numofHeart)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }


        }
    }


    //Recover 1 health every 10 seconds (if there is health loss)
    private void HealthRecovery()
    {
        if (health < numofHeart && health != 0)
        {
            recoverytimer += 1f * Time.deltaTime;

            if (recoverytimer > recoverydelay)
            {
                if (this.gameObject.CompareTag("Player"))
                {
                    health += 1;
                    recoverytimer = 0f;
                }
            }
        }



    }



}





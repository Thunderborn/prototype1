﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{

    [SerializeField]
    private float speed;
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        //Constantly move the projectile's position forward (z axis) with deltatime + speed considerations.
        transform.position += transform.forward * (Time.deltaTime * speed);



    }

    
    private void OnTriggerEnter(Collider other)
    {
        //Turn off the projectile collision after it hits the player to prevent double damage or complications
        OneDamageOnly(other);
       
    }

    //Destroy projectile if it isn't in camera view.
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }






    //No more projectile damage after hit.
    private void OneDamageOnly(Collider other)
    {
        //If player hit.
        if (other.transform.CompareTag("Player"))
        {
            //If projectile is sphere collider then disable its collider to prevent double hits.
            if (gameObject.GetComponent<SphereCollider>() == true)
            {
                gameObject.GetComponent<SphereCollider>().enabled = false;
            }


        }
    }







}




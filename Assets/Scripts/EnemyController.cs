﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    //Enemy health
    [SerializeField]
    private Health enemyhealth;



    //Player position
    private Transform playerpos;
    //Adjusted playerpos with Y increased.
    Vector3 fixedpos;

    [SerializeField]
    private GameObject projectile;
    private GameObject spawnedbullet;

    

    //For shooting
    private float shoottimer = 0f;
    [SerializeField]
    private float shootdelay;

    public float dist; 
    Transform enemypos;

    // Start is called before the first frame update
    void Start()
    {
        enemypos = transform;

        //Grab player position.
        playerpos = GameObject.FindWithTag("Player").GetComponent<Transform>();
        
        //Shoot delay.
        shoottimer = Time.timeSinceLevelLoad + shootdelay;



    }

    // Update is called once per frame
    void Update()
    {




        //Always get the position of player with elevated Y value so projectile doesn't target player origin.
        fixedpos = new Vector3(playerpos.position.x, playerpos.position.y + 2, playerpos.position.z);


        EnemyDeath();

        DistanceShoot();


        //Distance checker between player + enemy.
        //Debug.Log(dist);


    }



    private void shoot()
    {

        //Spawn bullet at enemy position
        spawnedbullet = Instantiate(projectile, enemypos.position, Quaternion.identity);
        //Make bullet look at player (rotation)
        spawnedbullet.transform.LookAt(fixedpos);

        //Make timer longer than 1 second to delay it for 1 second (since level load as the game restarts scene which continues timer)
        shoottimer = Time.timeSinceLevelLoad + shootdelay;


    }


    private void DistanceShoot()
    {
        dist = Vector3.Distance(playerpos.position, enemypos.position);

        //If distance is less than 30 and time is bigger than timer...
        if (dist < 30 && dist > 3 && Time.timeSinceLevelLoad > shoottimer)
        {
            //Inititate shoot function that shoots at player when in range.
            shoot();
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        //If any of these gameobject tags hit the enemy, take 1 damage.
        if (other.gameObject.CompareTag("Right") || other.gameObject.CompareTag("Left")
           || other.gameObject.CompareTag("Up") || other.gameObject.CompareTag("Down"))
        {
            enemyhealth.Damage(1);
            //Instantly disable box collider if enemy hit to prevent double hits
            other.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }

    private void EnemyDeath()
    {
        //Destroy obj if enemy health is 0
        if (enemyhealth.health == 0)
        {
            FindObjectOfType<AudioManager>().Play("EnemyDeath");
            Destroy(gameObject);

            if (PlayerController.enemieskilled != UIEnemiesKilled.enemylimit)
            {
                PlayerController.enemieskilled++;
            }
        }
    }


}

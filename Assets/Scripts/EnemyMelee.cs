﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMelee : MonoBehaviour
{
    //Enemy health
    [SerializeField]
    private Health enemyhealth;

    //Enemy position
    private Transform enemypos;

    //Player position
    private Transform playerpos;

    //Speed of enemy movement
    [SerializeField]
    private float speed;

    //Spike collision block
    private BoxCollider spikecollider;

    //Attempts to prevent double damage and only damages on true.
    public static bool damagecooldown = false;

    [SerializeField]
    private Animator animator;

    //For spike delay/cooldown
    private float spiketimer = 0f;
    private float spikedelay = 2f;

    //Grabs distance between player and enemy
    public float dist;


    // Start is called before the first frame update
    void Start()
    {
        //Grab enemy position.
        enemypos = transform;

        //Grab player position.
        playerpos = GameObject.FindWithTag("Player").GetComponent<Transform>();


        //Iterates through gameObject to find the child component, so cloned instances work and prevents manual assignment thru inspector.
        foreach (Transform child in this.transform)
        {
            if (child.CompareTag("Spikes"))
            {
                spikecollider = child.GetComponent<BoxCollider>();
            }

        }





    }

    // Update is called once per frame
    void Update()
    {


        EnemyDeath();
        Spikes();




    }



    private void Spikes()
    {
        //Grab dist between player + enemy
        dist = Vector3.Distance(playerpos.position, enemypos.position);

        //if distance between them is less than 3
        if (dist < 3)
        {
            //Spike attack cooldown timer.
            spiketimer += 1f * Time.deltaTime;

            Debug.Log("Spike enable");

            //If spiketimer is bigger/= than 1 and less than 1.01 (so we don't spam the sound play)
            if (spiketimer >= 1 && spiketimer < 1.01f )
            {
                animator.SetBool("attack", true);

                //Turn off damage cooldown (as playercontroller puts it to true if damaged)
                damagecooldown = false;

                //Disable collider box + reset timer to 0
                spikecollider.enabled = true;
                AudioManager.instance.Play("SpikeSound");

            }

            //If spiketimer is at 2 seconds or bigger..
            if (spiketimer >= spikedelay)
            {
                //Disable collider box + reset timer to 0
                spikecollider.enabled = false;
                spiketimer = 0;
                animator.SetBool("attack", false);
            }
        }
        else if (dist > 3 && dist < 30)
        {
            animator.SetBool("attack", false);
            move();
        }




    }


    private void OnTriggerEnter(Collider other)
    {
        //If any of these gameobject tags hit the enemy, take 1 damage.
        if (other.gameObject.CompareTag("Right") || other.gameObject.CompareTag("Left")
           || other.gameObject.CompareTag("Up") || other.gameObject.CompareTag("Down"))
        {
            enemyhealth.Damage(1);
            //Instantly disable box collider if enemy hit to prevent double hits
            other.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }

    private void EnemyDeath()
    {
        //Destroy obj if enemy health is 0
        if (enemyhealth.health == 0)
        {
            AudioManager.instance.Play("EnemyDeath");
            Destroy(gameObject);

            //Add one to objective if objective not complete yet.
            if (PlayerController.enemieskilled != UIEnemiesKilled.enemylimit)
            {
                PlayerController.enemieskilled++;
            }
        }

    }


    private void move()
    {
        //Move towards player.
        enemypos.position = Vector3.MoveTowards(enemypos.position, playerpos.position, speed * Time.deltaTime);
    }




}

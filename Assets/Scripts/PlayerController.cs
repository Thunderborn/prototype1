﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    //Player movement variables
    [SerializeField]
    public Rigidbody theRB;
    public float moveSpeed;

    //Records how many enemies you have killed.
    public static int enemieskilled;

    //Animation variables
    public Animator animator;
    private Vector3 moveInput;
    private string currentState;

    //Debuff conditions +  timer
    public bool canMove = true;
    private bool EnableDisarmTimer = false;
    private bool EnabledSlowedTimer = false;

    //Text indicators enabled to indicate to players of a debuff..
    [SerializeField]
    private GameObject disarmedtext;
    [SerializeField]
    private GameObject slowedtext;


    public SpriteRenderer theSR;


    //Disarm delay.
    private float playerdisarmtimer = 0f;
    public float playerdisarmdelay = 5f;
    private bool disarmcooldown = false;

    //Slowed delay
    private float playerslowtimer = 0f;
    public float playerslowdelay = 5f;
    private bool slowcooldown = false;

    //Grab player's health script
    private Health playerhealth;

    //Animation States
    const string PLAYER_IDLE_BACK  = "Idle_Back";
    
    const string PLAYER_RUN_BACK  = "Run_Back";
    const string PLAYER_RUN_FORW  = "Run_Forw";
    const string PLAYER_RUN_LEFT  = "Run_Left";
    const string PLAYER_RUN_RIGHT = "Run_Right";
    const string PLAYER_RUN_FORW_SLOW_RIGHT = "Run_Forw_Slow_Right";
    const string PLAYER_RUN_BACK_SLOW_RIGHT = "Run_Back_Slow_Right";
    const string PLAYER_RUN_FORW_SLOW_LEFT  = "Run_Forw_Slow_Left";
    const string PLAYER_RUN_BACK_SLOW_LEFT  = "Run_Back_Slow_Left";

    const string HIT_BACK  = "Hit_Back";
    const string HIT_FORW  = "Hit_Forw";
    const string HIT_LEFT  = "Hit_Left";
    const string HIT_RIGHT = "Hit_Right";



    // Start is called before the first frame update
    void Start()
    {
        //Since it's static, it will carry on through game restarts, so we force it to 0.
        enemieskilled = 0;

        theRB = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        playerhealth = GetComponent<Health>();

        


    }

    // Update is called once per frame
    void Update()
    {

        //Disables movement if player is attacking, else enables it.
        CanWeMove();

        //Movement 
        Move();
        MovementAnimation();

        //Player debuff cooldowns
        DisarmCooldown();
        SlowedCooldown();



    }


    private void Move()
    {
        //Enables movement if canMove is true
        EnableMovement();
        moveInput.Normalize();

        theRB.velocity = new Vector3(moveInput.x * moveSpeed, theRB.velocity.y, moveInput.y * moveSpeed);

        animator.SetFloat("moveSpeed", theRB.velocity.magnitude);

    }

    private void MovementAnimation()
    {
        // Up-Down-Left-Right Animation

        //
        if (moveInput.x == 1 && moveInput.y == 0)
        {
            ChangeAnimationState(PLAYER_RUN_RIGHT);
        }
        //
        if (moveInput.x == -1 && moveInput.y == 0)
        {
            ChangeAnimationState(PLAYER_RUN_LEFT);
        }
        //
        if (moveInput.x == 0 && moveInput.y == -1)
        {
            ChangeAnimationState(PLAYER_RUN_FORW);
        }
        //
        if (moveInput.x == 0 && moveInput.y == 1)
        {
            ChangeAnimationState(PLAYER_RUN_BACK);
        }
        //
        if (moveInput.x == 0 && moveInput.y == 0)
        {
            ChangeAnimationState(PLAYER_IDLE_BACK);
        }



        // Diagonal Animation
        
        // LEFT AND UP
        if (moveInput.x > 0 && moveInput.x < 1 && moveInput.y < 0 && moveInput.y > -1)
        {
            ChangeAnimationState(PLAYER_RUN_FORW_SLOW_LEFT);
        }
        // RIGHT AND UP
        if (moveInput.x < 0 && moveInput.x > -1 && moveInput.y < 0 && moveInput.y > -1)
        {
            ChangeAnimationState(PLAYER_RUN_FORW_SLOW_RIGHT);
        }
        // LEFT AND DOWN
        if (moveInput.x > 0 && moveInput.x < 1 && moveInput.y > 0 && moveInput.y < 1)
        {
            ChangeAnimationState(PLAYER_RUN_BACK_SLOW_LEFT);
        }
        // RIGHT AND DOWN
        if (moveInput.x < 0 && moveInput.x > -1 && moveInput.y > 0 && moveInput.y < 1)
        {
            ChangeAnimationState(PLAYER_RUN_BACK_SLOW_RIGHT);
        }

    }



    
    private void OnTriggerEnter(Collider other)
    {
        CollisionList(other);
    }

    public void ChangeAnimationState(string newState)
    {
        //stop the same animation from interrupting itself
        if (currentState == newState) return;

        //play the animation
        animator.Play(newState);

        //reassign the current state
        currentState = newState;
    }

    private void CanWeMove()
    {
        //Disable movement when attacking OR when health is 0
        if (AttackTrigger.busy == true || playerhealth.health <=0)
        {
            canMove = false;
        }
        else
        {
            canMove = true;
        }
    }

    private void EnableMovement()
    {
        //Checking for inputs. Used enable/disable movement
        if (canMove)
        {
            moveInput.x = Input.GetAxisRaw("Horizontal");


            moveInput.y = Input.GetAxisRaw("Vertical");
        }
    }

    private void DisarmCooldown()
    {
        if (EnableDisarmTimer)
        {
            playerdisarmtimer += 1f * Time.deltaTime;

            if (playerdisarmtimer > playerdisarmdelay)
            {
                //Set disarmed to false in another script so player can attack
                AttackTrigger.disarmed = false;
                //Reset timer
                playerdisarmtimer = 0f;
                EnableDisarmTimer = false;

                //Disable text above players head to indicate that they're not disarmed anymore.
                disarmedtext.SetActive(false);

            }
        }


    }

    private void SlowedCooldown()
    {
        //Adds slow effect to player with a timer delay.
        if (EnabledSlowedTimer)
        {
            slowcooldown = true;
            //Starts the timer
            playerslowtimer += 1f * Time.deltaTime;

            if (playerslowtimer > playerslowdelay)
            {
                moveSpeed = 20;
                playerslowtimer = 0f;
                EnabledSlowedTimer = false;
                slowedtext.SetActive(false);
                slowcooldown = false;

            }
           
        }


    }



    private void CollisionList(Collider other)
    {
        //Ranged enemy projectile
        if (other.gameObject.CompareTag("Projectile"))
        {
            Debug.Log("Projectile Hit");
            AudioManager.instance.Play("PlayerDamage");
            playerhealth.Damage(1);

        }

        //Damage cooldown is to prevent double hits on the player
        if (other.gameObject.CompareTag("Spikes") && !EnemyMelee.damagecooldown)
        {
            Debug.Log("Spike hit");
            AudioManager.instance.Play("PlayerDamage");
            playerhealth.Damage(1);
            EnemyMelee.damagecooldown = true;


        }

        //Boss projectiles
        if (other.gameObject.CompareTag("DamageBall"))
        {
            Debug.Log("Damage ball");
            AudioManager.instance.Play("PlayerDamage");
            playerhealth.Damage(1);

        }

        //Slow player down for 5 seconds and regain back speed.
        if (other.gameObject.CompareTag("SlowBall"))
        {
            Debug.Log("Slow ball");
            if (!slowcooldown)
            {
                slowedtext.SetActive(true);
                AudioManager.instance.Play("PlayerDamage");
                moveSpeed -= 10;
                EnabledSlowedTimer = true;
            }


        }

        //Disallow the player to attack for 5 seconds.
        if (other.gameObject.CompareTag("DisarmBall"))
        {
            Debug.Log("Disarm ball");

            if (!disarmcooldown)
            {
                AttackTrigger.disarmed = true;
                //Enable disarmed text above player for indication

                disarmedtext.SetActive(true);

                //Timer only works in update for disarm debuff timer.
                EnableDisarmTimer = true;

                //Play damaged audio.
                AudioManager.instance.Play("PlayerDamage");
            }



        }
    }



}

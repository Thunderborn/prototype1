﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossController : MonoBehaviour
{
    //Enemy health
    [SerializeField]
    public Health enemyhealth;

    //Player position
    private Transform playerpos;
    private Rigidbody playerRB;

    //Boss destroy delay
    [SerializeField]
    private float lifetime;

    public GameObject[] projectile;
    private GameObject spawnedbullet;

    //For shooting delays
    private float shoottimer = 0f;
    [SerializeField]
    private float shootdelay;

    //For next projectile delay
    private float nextballtimer = 0f;
    private float nextballdelay = 5f;

    //Distance between boss and player
    public float dist;

    //Boss location + adjusted projectile location
    Transform enemypos;

    //Animations.
    public Animator animator;
    public Transform CloakOB;

    private string currentState;

    //Animation States
    const string CLOAK = "Cloak";
    const string DAMAGE = "Damage";
    const string DEATH = "Death";
    const string DISARM = "Disarm";
    const string SLOW = "Slow";


    //If true, change boss projectiles to a random one
    private static bool nextball = false;

    //Index to grab the random projectile from array.
    private int ballindex;



    // Start is called before the first frame update
    void Start()
    {

        //Grab components
        enemypos = transform.GetChild(3).GetComponent<Transform>();

        //Grab player position.
        playerpos = GameObject.FindWithTag("Player").GetComponent<Transform>();
        playerRB = GameObject.FindWithTag("Player").GetComponent<Rigidbody>();


        //Shoot delay.
        shoottimer = Time.timeSinceLevelLoad + shootdelay;

        //Grab child component Cloak
        CloakOB = transform.GetChild(2);

    }

    // Update is called once per frame
    void Update()
    {


        //If player completed the enemies killed objective...
        if (PlayerController.enemieskilled == UIEnemiesKilled.enemylimit)
        {
            //Always check if boss is on cooldown
            nextballcooldown();

            EnemyDeath();

            DistanceShoot();


            //Distance checker between player + enemy.
            //Debug.Log(dist);
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            PlayerController.enemieskilled = 10;
        }


    }

    private void shoot()
    {


        if (!nextball)
        {
            ballindex = Random.Range(0, projectile.Length);

        }

        //Grab a random projectile ball from array
        GameObject randomprojectile = projectile[ballindex];

        if (!nextball)
        {
            BossCastAnimation(randomprojectile);

        }


        
        
        //Spawn bullet at random direction, locking the X axis.
        spawnedbullet = Instantiate(randomprojectile, enemypos.position, Quaternion.Euler(0, Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f))) ;


        nextball = true;
        //Make timer longer than 1 second to delay it for 1 second 
        shoottimer = Time.timeSinceLevelLoad + shootdelay;


    }


    private void DistanceShoot()
    {
        dist = Vector3.Distance(playerpos.position, enemypos.position);


        //If distance is less than 30 and time is bigger than timer...
        if (dist < 60 && Time.timeSinceLevelLoad > shoottimer)
        {
            //Inititate shoot function that shoots at player when in range.
            shoot();
        }



    }

    private void OnTriggerEnter(Collider other)
    {
        //Boss cannot be killed if the objectives are not met and the player somehow accesses the boss
        if (PlayerController.enemieskilled == UIEnemiesKilled.enemylimit)
        {
            //If any of these gameobject tags hit the enemy, take 1 damage.
            if (other.gameObject.CompareTag("Right") || other.gameObject.CompareTag("Left")
               || other.gameObject.CompareTag("Up") || other.gameObject.CompareTag("Down"))
            {
                enemyhealth.Damage(1);
                //Instantly disable box collider if enemy hit to prevent double hits
                other.gameObject.GetComponent<BoxCollider>().enabled = false;

            }
        }

    }

    private void EnemyDeath()
    {
        //Destroy obj if enemy health is 0
        if (enemyhealth.health == 0)
        {
            transform.GetChild(2).gameObject.SetActive(false);
            ChangeAnimationState(DEATH);
            AudioManager.instance.Play("BossDeath");
            Destroy(gameObject, lifetime);

        }
    }

    private void BossCastAnimation(GameObject randomprojectile)
    {

        if (randomprojectile.CompareTag("SlowBall"))
        {
            ChangeAnimationState(SLOW);
        }
        else if (randomprojectile.CompareTag("DamageBall"))
        {
            ChangeAnimationState(DAMAGE);
        }
        else if (randomprojectile.CompareTag("DisarmBall"))
        {
            ChangeAnimationState(DISARM);
        }


    }

    public void ChangeAnimationState(string newState)
    {
        //stop the same animation from interrupting itself
        if (currentState == newState) return;

        //play the animation
        animator.Play(newState);

        //reassign the current state
        currentState = newState;
    }


    private void nextballcooldown()
    {
        //Change balls every 5 seconds.
        if (nextball)
        {
            nextballtimer += 1f * Time.deltaTime;

            if (nextballtimer > nextballdelay)
            {
                nextball = false;
                nextballtimer = 0f;
            }
        }


    }


}

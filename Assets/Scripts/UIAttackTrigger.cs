﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAttackTrigger : MonoBehaviour
{

    //Setting the variable to true will make the player attack at the specified direction.
    public void SetTriggerLeft()
    {
        if (!AttackTrigger.busy)
        {
            AttackTrigger.left = true;
        }
    }
    public void SetTriggerRight()
    {
        if (!AttackTrigger.busy)
        {
            AttackTrigger.right = true;
        }

    }
    public void SetTriggerDown()
    {
        if (!AttackTrigger.busy)
        {
            AttackTrigger.down = true;
        }
    }
    public void SetTriggerUp()
    {
        if (!AttackTrigger.busy)
        {
            AttackTrigger.up = true;
        }
    }
}

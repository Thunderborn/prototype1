﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTrigger : MonoBehaviour
{

    //The collider that would be used to detect if it hits an enemy
    private BoxCollider colliders;
    private PlayerController player;

    //If true, player cannot attack.
    public static bool disarmed = false;

    [SerializeField]
    private Animator animator;
    private string currentState;
    const string PLAYER_IDLE_BACK = "Idle_Back";
    const string HIT_BACK = "Hit_Back";
    const string HIT_FORW = "Hit_Forw";
    const string HIT_LEFT = "Hit_Left";
    const string HIT_RIGHT = "Hit_Right";

    public static bool left = false;
    public static bool right = false;
    public static bool down = false;
    public static bool up = false;


    //We want only one instance of this no matter how many objects this script is attached to.
    public static bool busy;

    // Start is called before the first frame update
    void Start()
    {
        //This forces busy to be false, as when the scene reloads a bug occurs if you die during the cooldown, which forces it to be true forever.
        busy = false;

        //Reference the variables to appropriate components.
        colliders = gameObject.GetComponent<BoxCollider>();

        //Reference the script from Player that has it attached.
        player = GameObject.Find("Player").GetComponent<PlayerController>();



    }

    // Update is called once per frame
    void Update()
    {
        mousepositionattack();


    }

    private IEnumerator AttackDirection(string directiontag, string animation, int delay = 1)
    {
        //If script is attached to gameObject with a parameter tag and it isn't 'busy'
        if (gameObject.CompareTag(directiontag) && !busy )
        {
            //If animation name is (parameter)
            if (player.animator.GetCurrentAnimatorStateInfo(0).IsName(PLAYER_IDLE_BACK) && !disarmed)
            {
                //Temp turn on busy & enable trigger collider on player
                busy = true;

                //Enable the collider.
                colliders.enabled = true;

                //Play correct direction animation
                ChangeAnimationState(animation);
                
                AudioManager.instance.Play("Hit");


                //Wait 1 second 
                yield return new WaitForSeconds(delay);

                //Disable collider
                colliders.enabled = false;

                //Disable all trigger static variables.
                DisableTrigger();

                //Revert back to idle animation.
                ChangeAnimationState(PLAYER_IDLE_BACK);

                //We're not attacking anymore.
                busy = false;

            }

        }
    }



    private Vector3 mouseposition()
    {

        //Viewport coordinates go from 0.0 in the lower left corner of the screen to 1.0 in the upper right of the screen.
        Vector3 mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

        //Give both axis a different offset  so they don't overlap each other.
        mousePos.x -= 0.5f;
        mousePos.y += 1.5f;

        Debug.Log(mousePos);

        return mousePos;

    }




    private void mousepositionattack()
    {
        //Enables colliders dependant on mouse click position
        if (!colliders.enabled && !busy )
        {
            //If  mouseposition y is positioned left the player + not busy 
            if (left )
            {

                StartCoroutine(AttackDirection("Left", HIT_LEFT));
                
            }
            //If  mouseposition y is positioned right the player + not busy
            else if (right)
            {
                StartCoroutine(AttackDirection("Right", HIT_RIGHT));

            }

            //If  mouseposition y is positioned below the player + not busy 
            if (down)
            {
                StartCoroutine(AttackDirection("Down", HIT_BACK));

            }
            //If mouseposition y is positioned above the player + not busy
            else if (up)
            {
                StartCoroutine(AttackDirection("Up", HIT_FORW));

            }

        }
    }

    void ChangeAnimationState(string newState)
    {
        //stop the same animation from interrupting itself
        if (currentState == newState) return;

        //play the animation
        animator.Play(newState);

        //reassign the current state
        currentState = newState;
    }

    private void DisableTrigger()
    {
        //Set all triggers false so it doesn't loop attack
        left = false;
        up = false;
        right = false;
        down = false;
    }


}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    //Find the boss in scene
    private BossController bosshealth;

    // Start is called before the first frame update
    void Start()
    {
        bosshealth = GameObject.FindWithTag("Boss").GetComponent<BossController>();
    }

    // Update is called once per frame
    void Update()
    {
        BossDeath();
        PlayerDeath();
    }



    private void WinScreen()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
    }



    private void BossDeath()
    {
        //Win screen if boss is dead after a 3 second delay.
        if (bosshealth.enemyhealth.health <= 0)
        {
            AudioManager.instance.StopPlaying("Ambience");
            Invoke("WinScreen", 3);
        }
    }


    private void PlayerDeath()
    {
        if (Health.deathsoundplay)
        {
            Debug.Log("Dead");
            //Stop playing the audio as it will play through death screen (as AudioManager has a DoNotDestroy command)
            AudioManager.instance.StopPlaying("Ambience");

            Invoke("DeathScreen", 2);
        }
    }

    private void DeathScreen()
    {
        //If called, load death scene.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }




}

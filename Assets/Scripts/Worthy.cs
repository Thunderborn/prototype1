﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worthy : MonoBehaviour
{
    GameObject worthytext;

    //For delays/
    private float timer;
    private float delay = 3f;

    void Start()
    {
        //Grab player's child component that includes the text.
        worthytext = GameObject.FindWithTag("Player").transform.GetChild(7).gameObject;

    }



    // Update is called once per frame
    private void Update()
    {

        displaytext();
        deletetext();


       
    }


    private void OnCollisionEnter(Collision collision)
    {
        //If objective not complete, the text isn't active and timer is reset to 0
        if (PlayerController.enemieskilled != UIEnemiesKilled.enemylimit && !worthytext.activeSelf && timer == 0)
        {
            //Set text active.
            worthytext.SetActive(true);
        }
    }

    //Set text to false if active after a few seconds delay.
    private void displaytext()
    {
        if (worthytext.activeSelf)
        {
            timer += 1f * Time.deltaTime;
            if (timer >= delay)
            {
                worthytext.SetActive(false);
                timer = 0f;

            }
        }
    }

    //Delete the text if objective complete as there is no need for it.
    private void deletetext()
    {
        if (PlayerController.enemieskilled == UIEnemiesKilled.enemylimit)
        {
            //Double destroy to prevent unity reference errors and make sure you cannot reference a deleted object.
            Destroy(worthytext);
            Destroy(gameObject);
            
        }
    }

}
